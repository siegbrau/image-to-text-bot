FROM openjdk:11-jre
ADD  /target/image-to-text-bot-0.0.1-SNAPSHOT.jar image-to-text-bot-0.0.1-SNAPSHOT.jar
EXPOSE 8083
ENTRYPOINT  ["java","-jar","image-to-text-bot-0.0.1-SNAPSHOT.jar"]
