# image-to-text-bot

This is a Telegram bot which can:

1. Read text from image and send it in separate message. Supports Russian and English languages. 
2. Write text on image by text request from user.

For retrieving text is used Python with [Tesseract OCR](https://github.com/tesseract-ocr/). Bot is made with Java Spring and to operate with Telegram API is used [Telegrambots](https://github.com/rubenlagus/TelegramBots)

## Setup

1. Create your bot in [Telegram](https://core.telegram.org/bots) and set botname and token in `application.properties`. 
2. Compile and run `mvn package`
3. Run app using JVM or use Dockerfile and create image.

## Examples
### Reading from the image:

<img src="/uploads/bf327b95d245e01981cd49cb362f3688/example.JPG"   height="500">

### File creation:

<img src="/uploads/2bd92df864530aace6f9d1507f0b3ad3/example_2.JPG"   height="480">
