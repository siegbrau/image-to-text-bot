package com.siegbrau.imagetotextbot;

import com.siegbrau.imagetotextbot.bot.Bot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootApplication
@EnableScheduling
@EnableAsync
public class ImageToTextBotApplication {
	private static final Logger LOGGER = LogManager.getLogger(ImageToTextBotApplication.class);

	public static void main(String[] args) {
		try {
			TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
			telegramBotsApi.registerBot(new Bot());
			LOGGER.info("Telegram bot instance created");
		} catch (TelegramApiException e) {
		  LOGGER.atError().log("Unable to create bot instance Error: ",e.getMessage());
		}
		SpringApplication.run(ImageToTextBotApplication.class, args);
	}

}
