package com.siegbrau.imagetotextbot.bot;

import com.siegbrau.imagetotextbot.bot.menu.MessageSender;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static com.siegbrau.imagetotextbot.bot.menu.CleanHelper.deleteTemporaryFiles;
import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.ADD_DESCRIPTION;
import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.ADD_INPUT_TEXT;

@Component
public class Bot extends TelegramLongPollingBot {
	private static final Logger LOGGER = LogManager.getLogger(Bot.class);

	@Value("${telegram.botname}")
	private String username;

	@Value("${telegram.token}")
	private String token;

	@Value("${general.folderpath}")
	private String folderPath;

	private String inputText = "";
	private String description = "";

	private volatile boolean inputFlag = false;
	private volatile boolean descFlag = false;

	@Override
	public String getBotUsername() {
		return username;
	}

	@Override
	public String getBotToken() {
		return token;
	}

	@Override
	public void onUpdateReceived(Update update) {
		MessageSender messageSender = new MessageSender();
		try {

			if (update.getMessage().hasPhoto()) {
				String currentFilepath = retrievePhoto(update);
				execute(messageSender
						.sendImageInterpretation(update.getMessage().getChatId().toString(),
								currentFilepath));
				refresh();
			}
			inputText =
					setTextWhenInputFlagIsActive(update.getMessage().getText());
			description =
					setTextWhenDescFlagIsActive(update.getMessage().getText());

			if ("/text".equals(update.getMessage().getText())) {
				inputFlag = true;
				execute(messageSender
						.sendMessageInfo(update.getMessage().getChatId().toString(),
								ADD_INPUT_TEXT));
			}

			if ("/desc".equals(update.getMessage().getText())) {
				descFlag = true;
				execute(messageSender
						.sendMessageInfo(update.getMessage().getChatId().toString(),
								ADD_DESCRIPTION));
			}

			//            When both parameters are set
			if (!description.isBlank() && !inputText.isBlank()) {
				execute(messageSender
						.sendMessageByRequest(update.getMessage().getChatId().toString(),
								description, inputText));
				refresh();
			}

		} catch (TelegramApiException | FileNotFoundException e) {
			LOGGER.error("Unable to send message {}", e.getMessage());
		} catch (IOException e) {
			LOGGER.error("Unable to send remove files {}", e.getMessage());
		}
	}

	private String retrievePhoto(Update update) throws TelegramApiException {
		PhotoSize photoSize = update.getMessage().getPhoto().get(1);
		GetFile getFile = GetFile.builder()
								 .fileId(photoSize.getFileId())
								 .build();
		//        filePath = photos/filename.format
		String filePath = (execute(getFile).getFilePath());
		File targetFile = new File(folderPath + filePath);
		downloadFile(filePath, targetFile);
		return targetFile.getAbsolutePath();
	}

	private String setTextWhenInputFlagIsActive(String update) {
		if (inputFlag) {
			inputFlag = false;
			return update;
		}
		return inputText;
	}

	private String setTextWhenDescFlagIsActive(String update) {
		if (descFlag) {
			descFlag = false;
			return update;
		}
		return description;
	}

	@Async
	void refresh() throws IOException {
		inputText = "";
		description = "";
		inputFlag = false;
		descFlag = false;
		deleteTemporaryFiles();
	}
}
