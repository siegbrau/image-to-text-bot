package com.siegbrau.imagetotextbot.bot.menu;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
public final class CleanHelper {

	public static void deleteTemporaryFiles() throws IOException {
		Files.deleteIfExists(Path.of("requested_image.jpg"));
		Files.deleteIfExists(Path.of("text_photo.txt"));
		Files.deleteIfExists(Path.of("created.jpg"));
	}

}
