package com.siegbrau.imagetotextbot.bot.menu;

import com.siegbrau.imagetotextbot.services.draw.DrawService;
import com.siegbrau.imagetotextbot.services.draw.impl.DefaultDrawServiceImpl;
import com.siegbrau.imagetotextbot.services.interpret.Interpreter;
import com.siegbrau.imagetotextbot.services.interpret.impl.ImageInterpreter;
import com.siegbrau.imagetotextbot.services.interpret.impl.TextInterpreter;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;
import org.telegram.telegrambots.meta.api.objects.InputFile;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.HELP_TEXT;
import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.WARNING;

public class MessageSender {

	public SendMessage sendHelp(String chatId) {
		return SendMessage.builder()
						  .chatId(chatId)
						  .text(HELP_TEXT)
						  .build();
	}

	public SendMessage sendImageInterpretation(String chatId, String filepath) {
		Interpreter interpreter = new ImageInterpreter();
		String result = interpreter.interpret(filepath);
		return SendMessage.builder()
						  .text(result)
						  .chatId(chatId)
						  .build();
	}
	public SendMessage warning(String chatId) {
		return SendMessage.builder()
						  .text(WARNING)
						  .chatId(chatId)
						  .build();
	}



	public SendPhoto sendMessageByRequest(String chatId, String text, String inputText) throws FileNotFoundException {
		String imageFoundPath  = srcOfRequestedFile(text);
		String result = srcOfDrawnImage(imageFoundPath,inputText);
		InputStream inputStream =
				new BufferedInputStream(new FileInputStream(result));
		InputFile photoResult = new InputFile(inputStream, result);
		return SendPhoto.builder()
						.photo(photoResult)
//                        .caption("Text: "+text +" Input text: "+inputText)
						.chatId(chatId)
						.build();
	}
	public SendPhoto sendRequestedImage(String chatId, String text) throws FileNotFoundException {
		String result = srcOfRequestedFile(text);
		InputStream inputStream =
				new BufferedInputStream(new FileInputStream(result));
		InputFile photoResult = new InputFile(inputStream, result);
		return SendPhoto.builder()
						.photo(photoResult)
						.chatId(chatId)
						.build();
	}

	private static String srcOfRequestedFile(String text) {
		Interpreter interpreter = new TextInterpreter();
		return interpreter.interpret(text);
	}

	private static String srcOfDrawnImage(String imageFoundPath, String inputText){
		DrawService drawService = new DefaultDrawServiceImpl();
		return drawService.drawTextReturnPath(imageFoundPath,inputText);
	}

	public SendMessage sendMessageInfo(String chatId, String addInputText) {
		return SendMessage.builder()
						.text(addInputText)
						.chatId(chatId)
						.build();
	}
}
