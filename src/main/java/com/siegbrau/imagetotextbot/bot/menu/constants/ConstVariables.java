package com.siegbrau.imagetotextbot.bot.menu.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.awt.*;

@NoArgsConstructor(access = AccessLevel.PACKAGE)
public final class ConstVariables {
	public static final String HELP_TEXT = "";
	public static final String UNABLE_TO_FIND_IMAGE_BY_TEXT = "Why are we still here? Just to suffer?";
	public static final String WARNING = "Either input text or image is not set.";
	public static final String ADD_INPUT_TEXT = "Write your input text for image";
	public static final String ADD_DESCRIPTION = "Write your description text";
	public static final Font DEFAULT_FONT = new Font("Arial", Font.BOLD, 30);
}
