package com.siegbrau.imagetotextbot.services.draw;

import org.springframework.stereotype.Service;

@Service
public interface DrawService {
	String drawTextReturnPath(String imagePath, String text);
}
