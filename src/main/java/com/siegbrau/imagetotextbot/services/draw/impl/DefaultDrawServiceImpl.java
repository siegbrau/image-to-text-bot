package com.siegbrau.imagetotextbot.services.draw.impl;

import com.siegbrau.imagetotextbot.services.draw.DrawService;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.DEFAULT_FONT;

@Service
public class DefaultDrawServiceImpl implements DrawService {
	private static final Logger LOGGER = LogManager.getLogger(DefaultDrawServiceImpl.class);

	@Override
	public String drawTextReturnPath(String imagePath, String text) {
		ImagePlus image = IJ.openImage(imagePath);
		ImageProcessor ip = image.getProcessor();
		ip.setFont(DEFAULT_FONT);
		ip.drawString(text, 15, image.getHeight()-40);
		Image created = ip.createImage();
		File file = new File("created.jpg");
		try {
			ImageIO.write((RenderedImage) created, "jpg", file);
		} catch (IOException e) {
			LOGGER.error("Unable to create image. See exception {}", e.getMessage());
		}
		return file.getAbsolutePath();
	}
}
