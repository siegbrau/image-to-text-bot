package com.siegbrau.imagetotextbot.services.draw.impl;

import com.siegbrau.imagetotextbot.services.draw.DrawService;
import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;


import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.DEFAULT_FONT;

@Getter
@Setter
@AllArgsConstructor
public class SpecialDrawServiceImpl implements DrawService {

	private static final Logger LOGGER = LogManager.getLogger(SpecialDrawServiceImpl.class);
	private int fontSize;
    private int fontPositionX;
    private int fontPositionY;
    private String fontAlign="";
    // Bold, cursive, etc
    private String style;


    @Override
    public String drawTextReturnPath(String imagePath, String text) {
        ImagePlus image = IJ.openImage(imagePath);
        ImageProcessor ip = image.getProcessor();
        ip.setFont(prepareFont());
        setupAlignIfSpecified(image);
        ip.drawString(text, fontPositionX, fontPositionY);
        Image created = ip.createImage();
        File file = new File("created.jpg");
        try {
            ImageIO.write((RenderedImage) created, "jpg", file);
        } catch (IOException e) {
            LOGGER.error("Unable to create image. See exception {}", e.getMessage());
        }
        return file.getAbsolutePath();
    }

	private void setupAlignIfSpecified(ImagePlus image) {
    	if(fontAlign.isBlank()){
    		return;
		}
    	if(fontAlign.contains("right"))
    		fontPositionX = image.getWidth()-30;
    	if(fontAlign.contains("top")){
			fontPositionY = 30;
		}
		if(fontAlign.contains("left")){
			fontPositionY = 30;
		}
		if(fontAlign.contains("bottom")){
			fontPositionY = image.getHeight()-30;
		}
	}

	private Font prepareFont() {
    	switch (style.toLowerCase()){
			case "cursive":{
				return new Font("Courier New", Font.ITALIC, fontSize);
			}
			case "bold": {
				return new Font("Arial", Font.BOLD, fontSize);
			}
			case "plain": {
				return new Font("Sans", Font.PLAIN, fontSize);
			}
			default: return DEFAULT_FONT;
		}
    }
}
