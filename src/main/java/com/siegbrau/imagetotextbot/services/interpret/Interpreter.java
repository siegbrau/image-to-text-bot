package com.siegbrau.imagetotextbot.services.interpret;

public interface Interpreter {
	String interpret(String imagePath);
}
