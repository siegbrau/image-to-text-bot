package com.siegbrau.imagetotextbot.services.interpret.impl;

import com.siegbrau.imagetotextbot.services.interpret.Interpreter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageInterpreter implements Interpreter {

	private static final Logger LOGGER = LogManager.getLogger(ImageInterpreter.class);

	@Override
	public String interpret(String imagePath) {
		String result = "Unable to interpret text on the image.";
		if (!Files.exists(Paths.get(imagePath))) {
			return result;
		}

		ProcessBuilder processBuilder =
				new ProcessBuilder("python", resolvePythonScriptPath("interpret.py"), imagePath);
		try {
			Process process = processBuilder.start();
			process.waitFor();
			process.destroy();
			result = readFromFile();
			deleteFiles(imagePath);
		} catch (IOException | InterruptedException e) {
			LOGGER.fatal("Unable to read from image. Cause:{} \n Message:{} ", e.getCause(),
					e.getMessage());
		}

		return result;
	}

	private void deleteFiles(String imagePath) throws IOException {
		Files.delete(Paths.get(imagePath));
	}

	private String readFromFile() throws IOException {
		File file = new File("text_photo.txt");
		return Files.readString(Path.of(file.getAbsolutePath()));
	}

	private String resolvePythonScriptPath(String filename) {
		File file = new File("src/main/resources/scripts/" + filename);
		return file.getAbsolutePath();
	}
}
