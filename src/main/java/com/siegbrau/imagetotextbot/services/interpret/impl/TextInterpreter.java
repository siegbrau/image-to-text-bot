package com.siegbrau.imagetotextbot.services.interpret.impl;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.siegbrau.imagetotextbot.services.interpret.Interpreter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static com.siegbrau.imagetotextbot.bot.menu.constants.ConstVariables.UNABLE_TO_FIND_IMAGE_BY_TEXT;

@Service
public class TextInterpreter implements Interpreter {
	private static final Logger LOGGER = LogManager.getLogger(TextInterpreter.class);

	//    Returns imagePath
	@Override
	public String interpret(String text) {
		if (text.isBlank()) {
			return UNABLE_TO_FIND_IMAGE_BY_TEXT;
		}
		try {
			String imageUrl = retrieveLinkOfFirstImage(text);
			InputStream image = retrieveImageFromUrl(imageUrl);
			return writeToFile(image).toString();
		} catch (IOException e) {
			LOGGER.error("Unable to find image by following {} request. See error {}", text,
					e.getMessage());
			return UNABLE_TO_FIND_IMAGE_BY_TEXT;
		}
	}

	private Path writeToFile(InputStream image) throws IOException {
		File file = new File("requested_image.jpg");
		return Files.write(file.toPath(), image.readAllBytes());
	}

	private String retrieveLinkOfFirstImage(String text) throws IOException {
		WebClient webClient2 = new WebClient();
		setWebClient(webClient2);
		HtmlPage searchPage = webClient2.getPage(searchUrl(text, "https://duckduckgo.com/?q="));
//        wait 3 seconds for js page to launch
		synchronized (searchPage) {
			try {
				searchPage.wait(3000);
			} catch (InterruptedException e) {
			   LOGGER.error(e.getMessage(),e.getCause());
			}
		}
		Document doc = Jsoup.parse(searchPage.asXml());
		Elements images = doc.select("img");
//        don't use abs url since it'll return text, not link
		return "https:" + images.get(5).attr("src");
	}

	private void setWebClient(WebClient webClient2) {
		webClient2.getOptions().setJavaScriptEnabled(true);
		webClient2.getOptions().setCssEnabled(false);
		webClient2.waitForBackgroundJavaScript(3000);
	}

	private BufferedInputStream retrieveImageFromUrl(String url) throws IOException {
		return Jsoup.connect(url).ignoreContentType(true).execute().bodyStream();
	}

	//    duck duck go url looks like https://duckduckgo.com/?q=<>&iax=images&ia=images
	private String searchUrl(String text, String searchSource) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(searchSource);
		for (String word : text.split(" ")) {
			stringBuilder.append(word);
			stringBuilder.append("+");
		}
		stringBuilder.append("&iax=images&ia=images");
		return stringBuilder.toString();
	}
}
