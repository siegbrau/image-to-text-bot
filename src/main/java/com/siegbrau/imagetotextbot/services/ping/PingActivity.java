package com.siegbrau.imagetotextbot.services.ping;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Service

@Slf4j
public class PingActivity {

	private static String googleLink = "https://www.google.com";

	@Scheduled(fixedRateString =  "${schedule.time}")
	public void pingGoogle(){
		try {
			URL url = new URL(googleLink);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.connect();
			log.info("Ping {}, OK: response code {}", url.getHost(), connection.getResponseCode());
			connection.disconnect();
		} catch (IOException e) {
			log.error("Ping FAILED. See: {}", e.getMessage());
		}
	}

}
