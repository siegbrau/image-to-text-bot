# импортируем нужные библиотеки
import pytesseract
from PIL import Image
import sys, getopt
import codecs
pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract'
if __name__ == '__main__':
    args = sys.argv[1]
    f = codecs.open('text_photo.txt', 'w', 'utf-8')
    text = pytesseract.image_to_string(Image.open(args), lang='rus+eng')
    # записываем его
    f.write(text)
    f.close()
    print(pytesseract.image_to_string(Image.open(args), lang='rus+eng'))
