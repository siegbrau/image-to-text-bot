package com.siegbrau.imagetotextbot.bot;

import com.siegbrau.imagetotextbot.bot.menu.MessageSender;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MessageSenderTest {
    private static final MessageSender messageSender = new MessageSender();
    private static final String chatId = "1234567890";

    private static File file = new File("src/test/resources/temp/photos/test_1.jpg");

    @BeforeAll
    public static void setUp() throws IOException {
        file.createNewFile();
        String source = "src/test/resources/temp/photos/test.jpg";
        Files.write(file.toPath(), new FileInputStream(source).readAllBytes());
    }

    @Test
    public void verifyCreatesInterpretationImageMessage() {
        SendMessage sendMessage = messageSender.sendImageInterpretation(chatId, file.getPath());
        Assertions.assertNotNull(sendMessage,"Message is null");
        Assertions.assertFalse(sendMessage.getText().isBlank(),"Message is empty");
    }
    @Test
    public void verify() throws FileNotFoundException {
        SendPhoto sendPhoto = messageSender.sendMessageByRequest(chatId, "кот", "meow-meow");
        String filePath = sendPhoto.getPhoto().getAttachName().replaceAll("attach://","");
        Assertions.assertTrue(Files.exists(Path.of(filePath)));
    }

	@AfterAll
	public static void DeleteCreatedFiles() throws IOException {
		Files.deleteIfExists(Path.of("requested_image.jpg"));
		Files.deleteIfExists(Path.of("text_photo.txt"));
	}
}
