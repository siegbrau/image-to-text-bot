package com.siegbrau.imagetotextbot.services.draw;

import com.siegbrau.imagetotextbot.services.draw.impl.DefaultDrawServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultDrawServiceImplTests {
	private final static File file = new File("src/test/resources/temp/photos/test.jpg");
	private final static String text = "Why are you doing this?";
	private DrawService drawService = new DefaultDrawServiceImpl();

	@Test
	public void VerifyFileIsCreated() {
		String path = drawService.drawTextReturnPath(file.getPath(), text);
		assertThat(Files.exists(Paths.get(path)));
	}

	@Test
	public void VerifyServiceReturnsPath() {
		String path = drawService.drawTextReturnPath(file.getPath(), text);
		Assert.notNull(path, "No path is returned");
		Assertions.assertNotNull(path);
	}

}
