package com.siegbrau.imagetotextbot.services.interpret;

import com.siegbrau.imagetotextbot.services.interpret.impl.ImageInterpreter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ImageInterpreterTest {
	private static final Interpreter interpreter = new ImageInterpreter();

	private static File file = new File("src/test/resources/temp/photos/test_1.jpg");

	@BeforeAll
	public static void setUp() throws IOException {
		file.createNewFile();
		String source = "src/test/resources/temp/photos/test.jpg";
		Files.write(file.toPath(), new FileInputStream(source).readAllBytes());
	}

	@Test
	public void verifyTextWasInterpret() {
		String text = interpreter.interpret(file.getPath());
		Assertions.assertFalse(text.isBlank(), "Message is empty");
	}

	@AfterAll
	public static void DeleteCreatedFiles() throws IOException {
		Files.deleteIfExists(Path.of("text_photo.txt"));
	}
}
