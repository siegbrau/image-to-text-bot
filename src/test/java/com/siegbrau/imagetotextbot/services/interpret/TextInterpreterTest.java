package com.siegbrau.imagetotextbot.services.interpret;

import com.siegbrau.imagetotextbot.services.interpret.impl.TextInterpreter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TextInterpreterTest {

	private String simpleRequest = "Весемир";
	private String complicatedRequest = "Злой Весемир, с мечом";
	private Interpreter interpreter;

	@BeforeEach
	private void SetUp(){
		interpreter = new TextInterpreter();
	}

	@Test
	public void verifySimpleTextWasInterpreted(){
		String imagePath = interpreter.interpret(simpleRequest);
		Assert.notNull(imagePath, "No path is returned");
	}
	@Test
	public void verifyComplexTextWasInterpreted(){
		String imagePath = interpreter.interpret(complicatedRequest);
		Assert.notNull(imagePath, "No path is returned");
	}

	@AfterEach
	public void DeleteCreatedFiles() throws IOException {
		Files.delete(Paths.get("requested_image.jpg"));
	}
}
